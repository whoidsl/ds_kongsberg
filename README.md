# ds_kongsberg

This is the repository for the ros node controlling the Kongsberg EM2040 multibeam and associated files.

## catkin packages

- ds_kongsberg
- ds_kongsberg_msgs



## KCTRL    
Currently kctrl is able to be started through this driver by calling
a rosservice "KctrlCmd.srv"    "KM_KCTRL 1"
This has been tested and turned on over sonardyne sms on the sentry vehicle.

The implementation is limited to calling the following executable script:
 ~/sentry_ws/src/ds_kongsberg/ds_kongsberg/scripts/kctrl_tmux.bash

The executable script would have to exist at the path mentioned on the driver's
filesystem with this implementation.

This could be extended or modified to work with other vehicles and filesystems
