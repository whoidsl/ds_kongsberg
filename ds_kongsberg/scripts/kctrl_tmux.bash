#! /bin/bash

# the source setup line may not be explicitely needed. Tested with and this all works
source ~/sentry_ws/devel/setup.bash
tmux send-keys -t "sentry:kongsberg" "docker run --rm --network host kctrl 2>&1 | tee /data/kongsberg/log/remotek_ctrl.log" C-m
